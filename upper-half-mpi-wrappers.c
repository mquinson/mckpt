#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <mpi.h>

#include "common.h"
#include "upper-half-wrappers.h"
#include "upper-half-mpi-wrappers.h"

#undef MPI_Init
#undef MPI_Finalize
#undef MPI_Comm_rank
#undef MPI_Comm_size
#undef MPI_Send
#undef MPI_Recv
#undef MPI_Irecv
#undef MPI_Wait
#undef MPI_Wtime
#undef MPI_Barrier
#undef MPI_Allreduce

DEFINE_FNC(int, Init, (int *) argc, (char ***) argv)

DEFINE_FNC(int, Finalize, (void))

DEFINE_FNC(int, Comm_rank, (MPI_Comm) comm, (int *) world_rank)

DEFINE_FNC(int, Comm_size, (MPI_Comm) comm, (int *) size)

DEFINE_FNC(int, Send, (const void *) buf, (int) count, (MPI_Datatype) datatype,
           (int) dest, (int) tag, (MPI_Comm) comm)

DEFINE_FNC(int, Recv, (void *) buf, (int) count, (MPI_Datatype) datatype,
           (int) source, (int) tag, (MPI_Comm) comm, (MPI_Status *) status)

DEFINE_FNC(int, Wait, (MPI_Request *) request, (MPI_Status *) status)

DEFINE_FNC(double, Wtime, (void))

DEFINE_FNC(int, Irecv, (void *) buf, (int) count,
           (MPI_Datatype) datatype, (int) source,
           (int) tag, (MPI_Comm) comm, (MPI_Request *) request)

DEFINE_FNC(int, Barrier, (MPI_Comm) comm)

DEFINE_FNC(int, Allreduce, (const void *) sendbuf, (void *) recvbuf, (int) count,
           (MPI_Datatype) datatype, (MPI_Op) op, (MPI_Comm) comm)
