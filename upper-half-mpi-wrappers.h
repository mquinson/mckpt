#ifndef UPPER_HALF_MPI_WRAPPERS_H
#define UPPER_HALF_MPI_WRAPPERS_H

#define EAT(x)
#define REM(x) x
#define STRIP(x) EAT x
#define PAIR(x) REM x

/* This counts the number of args */
#define NARGS_SEQ(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, N, ...) N
#define NARGS(...) NARGS_SEQ(__VA_ARGS__, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1)

/* This will let macros expand before concating them */
#define PRIMITIVE_CAT(x, y) x ## y
#define CAT(x, y) PRIMITIVE_CAT(x, y)

/* This will call a macro on each argument passed in */
#define APPLY(macro, ...) CAT(APPLY_, NARGS(__VA_ARGS__))(macro, __VA_ARGS__)
#define APPLY_1(m, x1) m(x1)
#define APPLY_2(m, x1, x2) m(x1), m(x2)
#define APPLY_3(m, x1, x2, x3) m(x1), m(x2), m(x3)
#define APPLY_4(m, x1, x2, x3, x4) m(x1), m(x2), m(x3), m(x4)
#define APPLY_5(m, x1, x2, x3, x4, x5) m(x1), m(x2), m(x3), m(x4), m(x5)
#define APPLY_6(m, x1, x2, x3, x4, x5, x6) m(x1), m(x2), m(x3),                \
                                           m(x4), m(x5), m(x6)
#define APPLY_7(m, x1, x2, x3, x4, x5, x6, x7) m(x1), m(x2), m(x3),            \
                                               m(x4), m(x5), m(x6), m(x7)

#define NEXT_FUNC(func)                                                        \
  ({                                                                           \
    __typeof__(&MPI_##func)_real_MPI_## func = (__typeof__(&MPI_##func)) - 1;  \
    if (_real_MPI_ ## func == (__typeof__(&MPI_##func)) - 1) {                 \
      LhDlsym_t dlsymFptr = (LhDlsym_t)lhInfo.lhDlsym;                         \
      _real_MPI_ ## func = (__typeof__(&MPI_##func))dlsymFptr(MPI_Fnc_##func); \
    }                                                                          \
    _real_MPI_ ## func;                                                        \
  })

// Convenience macro to define simple wrapper functions
#define DEFINE_FNC(rettype, name, args...)                                     \
  rettype MPI_##name(APPLY(PAIR, args))                                        \
  {                                                                            \
    DLOG(SUPERNOISE, "In wrapper\n");                                          \
    rettype retval;                                                            \
    if (!initialized) {                                                        \
      initialize_wrappers();                                                   \
    }                                                                          \
    JUMP_TO_LOWER_HALF(lhInfo.lhFsAddr);                                       \
    retval = NEXT_FUNC(name)(APPLY(STRIP, args));                              \
    RETURN_TO_UPPER_HALF();                                                    \
    return retval;                                                             \
  }


// Redine MPI constants
#undef MPI_COMM_NULL
#undef MPI_COMM_WORLD
#undef MPI_COMM_SELF

#define MPI_COMM_NULL    0
#define MPI_COMM_WORLD   1
#define MPI_COMM_SELF    2

#undef MPI_BYTE
#undef MPI_INT
#undef MPI_LONG
#undef MPI_FLOAT
#undef MPI_DOUBLE

#define MPI_BYTE   0
#define MPI_INT    1
#define MPI_LONG   2
#define MPI_FLOAT  3
#define MPI_DOUBLE 4

#undef MPI_MIN
#undef MPI_MAX
#undef MPI_SUM
#undef MPI_PROD
#undef MPI_LAND
#undef MPI_BAND
#undef MPI_LOR
#undef MPI_BOR
#undef MPI_LXOR
#undef MPI_BXOR
#undef MPI_MINLOC
#undef MPI_MAXLOC
#undef MPI_REPLACE
#undef MPI_NO_OP

#define MPI_MIN       0
#define MPI_MAX       1
#define MPI_SUM       2
#define MPI_PROD      3
#define MPI_LAND      4
#define MPI_BAND      5
#define MPI_LOR       6
#define MPI_BOR       7
#define MPI_LXOR      8
#define MPI_BXOR      9
#define MPI_MINLOC    10
#define MPI_MAXLOC    11
#define MPI_REPLACE   12
#define MPI_NO_OP     13

extern int MPI_Init(int *argc, char ***argv) __attribute__((weak));
#define MPI_Init(f, c) (MPI_Init ? MPI_Init(f, c) : 0)

extern int MPI_Finalize() __attribute__((weak));
#define MPI_Finalize() (MPI_Finalize ? MPI_Finalize() : 0)

extern int MPI_Comm_rank(MPI_Comm comm, int *rank) __attribute__((weak));
#define MPI_Comm_rank(f, g) (MPI_Comm_rank ? MPI_Comm_rank(f, g) : 0)

extern int MPI_Comm_size(MPI_Comm comm, int *size) __attribute__((weak));
#define MPI_Comm_size(f, g) (MPI_Comm_size ? MPI_Comm_size(f, g) : 0)

extern int MPI_Send(const void *buf, int count,
                    MPI_Datatype datatype,
                    int dest, int tag,
                    MPI_Comm comm) __attribute__((weak));
#define MPI_Send(a, b, c, d, e, f) (MPI_Send ? MPI_Send(a, b, c, d, e, f) : 0)

extern int MPI_Recv(void *buf, int count, MPI_Datatype datatype,
                    int source, int tag,
                    MPI_Comm comm, MPI_Status *status) __attribute__((weak));
#define MPI_Recv(a, b, c, d, e, f, g) (MPI_Recv ? MPI_Recv(a, b, c, d, e, f, g) : 0)

extern int MPI_Irecv(void *buf, int count, MPI_Datatype datatype, int source,
                     int tag, MPI_Comm comm, MPI_Request *request);
#define MPI_Irecv(a, b, c, d, e, f, g) (MPI_Irecv ? MPI_Irecv(a, b, c, d, e, f, g) : 0)

extern int MPI_Wait(MPI_Request *request, MPI_Status *status);
#define MPI_Wait(a, b) (MPI_Wait ? MPI_Wait(a, b) : 0)

extern double MPI_Wtime(void);
#define MPI_Wtime() (MPI_Wtime ? MPI_Wtime() : 0.0)

extern int MPI_Barrier(MPI_Comm comm);
#define MPI_Barrier(a) (MPI_Barrier ? MPI_Barrier(a) : 0)

extern int MPI_Allreduce(const void *sendbuf, void *recvbuf, int count,
                         MPI_Datatype datatype, MPI_Op op, MPI_Comm comm);
#define MPI_Allreduce(a, b, c, d, e, f) (MPI_Allreduce ? MPI_Allreduce(a, b, c, d, e, f) : 0)

#endif // ifndef UPPER_HALF_MPI_WRAPPERS_H
